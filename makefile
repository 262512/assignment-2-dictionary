NASM=nasm
NFLAGS=-f elf64

%.o: %.asm
	$(NASM) $(NAFLAGS) -o $@ $<

program: main.o lib.o dict.o
	ld -o program $^

clean:
	rm -f *.o

.PHONY:
	clean
