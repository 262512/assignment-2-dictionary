%include 'colon.inc'
%include 'words.inc'
%include 'lib.inc'
%define BUF_SIZE 256
%define DQ_SIZE 8
extern find_word
section .data
    oversize: db "too long", 0
    no_such_key: db "no such key", 0
    invite: db: "Input:" 
section .text
global _start
_start:
    mov rdi, invite
    call print_string

    lea rdi, [rsp - BUF_SIZE]
    call read_string
    pop rdi
    test rax, rax
    push rax
    jz .out_of_bound
    
    mov rsi, LIST_HEAD
    call find_word
    test rax, rax
    jz .no_such_key
    jmp .print_by_key

    .print_by_key:

        mov rdi, rax
        add rdi, DQ_SIZE
        pop rax
        add rdi, rax
        
        call print_string
        call print_newline
        call exit

    .out_of_bound:
        mov rdi, oversize
        call print_error
        call exit

    .no_such_key:
        mov rdi, no_such_key
        call print_error
        call exit
