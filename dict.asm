%define DQ_SIZE 8

section .text

extern string_equals
extern print_string
global find_word
;rdi - указатель на значение, rsi - указатель на начало словаря
find_word:
    .loop:
        add rsi, DQ_SIZE ;указатель на след. элемент

        push rdi
        push rsi
        call string_equals
        pop rsi
        pop rdi
        sub rsi, DQ_SIZE

        test rax, rax
        jnz .found
        mov rsi, [rsi]
        test rsi, rsi
        jnz .loop

   .end:
        xor rax, rax
        ret
    .found:
        mov rax, rsi
        ret